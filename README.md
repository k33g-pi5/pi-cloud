# Pi Cloud template

## How to

```bash
git clone https://gitlab.com/k33g-pi5/pi-cloud.git
cd pi-cloud
docker compose build # if you change something into docker-dev-environment/Dockerfile
docker compose up
```

## Access to the workspaces

- http://localhost:3001/?tkn=ilovepandas&folder=/home/workspace-wasm
- http://localhost:3002/?tkn=ilovepandas&folder=/home/workspace-go
- http://localhost:3003/?tkn=ilovepandas&folder=/home/workspace-rust


## Start docker compose at boot (faking k33g user)

```bash
sudo nano /etc/rc.local
# add
cd /home/k33g/pi-cloud
su k33g -c 'docker compose up'
# do not remove `exit 0` at the end
sudo chmod +x /etc/rc.local
```

> 👋 replace `k33g` by your username