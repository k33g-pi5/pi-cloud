# Workspace (Wasm)

## Add a Workspace to GitLab (or somewhere else)

### SSH Key (GitLab.com)

```bash
ssh-keygen -t ed25519 -C "K33G_PI5_COLOSSUS"
cat /home/openvscode-server/.ssh/id_ed25519.pub
# register the publik key on GitLab.com
```

### Git config

```bash
git config --global user.name "Bob Morane"
git config --global user.email "bob@bots.garden"
```

## Put the project under GitLab

```bash
git init --initial-branch=main
git remote add origin git@gitlab.com:k33g-pi5/colossus-workspace.git
git add .
git commit -m "🎉 initialize"
git push --set-upstream origin main
```

